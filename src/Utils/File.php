<?php
namespace Crux\Utils;

class File
{
	/** @var resource */
	protected $handle;
	
	/** @var string */
	protected $path;
	
	/** @var string */
	protected $mode;
	
	const READ = 'r';
	const WRITE = 'w';
	const APPENDABLE = 'a';
	const READ_AND_WRITE = 'r+';
	const WRITE_AND_READ = 'w+';
	
	
	public function __construct($path, $mode = File::READ)
	{
		$this->path = $path;
		$this->mode = $mode;
		
		if ($mode) {
			$this->handle = fopen($path, $mode);
		}
	}
	
	public function copy($newPath) {
		copy($this->path, $newPath);
	}
	
	
	public function __destruct()
	{
		$this->close();
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return bool|File
	 */
	public static function create($path)
	{
		if (file_exists($path)) {
			return false;
		}
		
		return new self($path, File::WRITE);
	}
	
	
	/**
	 * @param string $path
	 * @param string $mode
	 *
	 * @return bool|File
	 */
	public static function load($path, $mode = File::READ)
	{
		if (! file_exists($path)) {
			return false;
		}
		
		return new self($path, $mode);
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return bool|string
	 */
	public static function readContent($path)
	{
		$file = new self($path, File::READ);
		
		$data = $file->read();
		
		$file->close();
		
		return $data;
	}
	
	
	public static function saveContent($path, $data)
	{
		$file = new self($path, File::WRITE);
		
		$file->write($data);
		$file->close();
	}
	
	
	/**
	 * @param string|mixed $data
	 */
	public function write($data)
	{
		if ($this->isWritable() or $this->isAppendable()) {
			fwrite($this->handle, $data);
		}
	}
	
	
	/**
	 * @param null|int $length
	 *
	 * @return bool|string
	 */
	public function read($length = null)
	{
		if ($length === null) {
			$length = filesize($this->path);
		}
		
		return fread($this->handle, $length);
	}
	
	
	public function delete()
	{
		$this->close();
		@unlink($this->path);
	}
	
	
	/**
	 * @return array
	 */
	public function stat()
	{
		return fstat($this->handle);
	}
	
	
	/**
	 * @return bool
	 */
	public function isWritable()
	{
		return (
			$this->mode === File::WRITE
			or $this->mode === File::READ_AND_WRITE
			or $this->mode === File::WRITE_AND_READ
		);
	}
	
	
	/**
	 * @return bool
	 */
	public function isAppendable()
	{
		return (strpos($this->mode, 'a') !== false);
	}
	
	
	/**
	 * 
	 */
	public function close()
	{
		@fclose($this->handle);
	}
}

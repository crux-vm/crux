<?php
namespace Crux\Utils;

class Path
{
	/**
	 * @param string $path
	 *
	 * @return bool
	 */
	public static function exists($path)
	{
		return realpath($path) ? true : false;
	}
	
	
	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public static function pretty($path)
	{
		if (self::exists($path)) {
			return realpath($path);
		} else {
			return $path;
		}
	}
	
	
	/**
	 * @param string[] ...$parts
	 *
	 * @return string
	 */
	public static function join(...$parts)
	{
		return implode(DIRECTORY_SEPARATOR, $parts);
	}
	
	
	/**
	 * 
	 * Find the relative file system path between two file system paths
	 *
	 * @param  string $fromPath Path to start from
	 * @param  string $toPath   Path we want to end up in
	 *
	 * @return string             Path leading from $frompath to $topath
	 */
	public static function getRelative($fromPath, $toPath)
	{
		$from = explode(DIRECTORY_SEPARATOR, $fromPath); // Folders/File
		$to = explode(DIRECTORY_SEPARATOR, $toPath); // Folders/File
		$relpath = '';
		
		$i = 0;
		// Find how far the path is the same
		while (isset($from[$i]) && isset($to[$i])) {
			if ($from[$i] != $to[$i]) {
				break;
			}
			$i++;
		}
		$j = count($from) - 1;
		// Add '..' until the path is the same
		while ($i <= $j) {
			if (! empty($from[$j])) {
				$relpath .= '..'.DIRECTORY_SEPARATOR;
			}
			$j--;
		}
		// Go to folder from where it starts differing
		while (isset($to[$i])) {
			if (! empty($to[$i])) {
				$relpath .= $to[$i].DIRECTORY_SEPARATOR;
			}
			$i++;
		}
		
		// Strip last separator
		return substr($relpath, 0, -1);
	}
}

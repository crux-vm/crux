<?php
namespace Crux;

use Symfony\Component\Console\Application;

class ConsoleApplication
{
	const APP_NAME = 'Crux';
	const APP_VERSION = '0.0.1';
	
	/** @var Application */
	protected $app;
	
	
	/**
	 * ConsoleApplication constructor.
	 */
	public function __construct()
	{
		$this->app = new Application(self::APP_NAME, self::APP_VERSION);
		
		$this->setupCommands();
	}
	
	
	/**
	 * 
	 */
	public function run()
	{
		$this->app->run();
	}
	
	
	/**
	 * 
	 */
	protected function setupCommands()
	{
		$this->loadCommand(new Vagrant\Command\InitCommand);
	}
	
	
	/**
	 * @param Command $command
	 */
	protected function loadCommand(Command $command)
	{
		$this->app->add($command);
	}
}

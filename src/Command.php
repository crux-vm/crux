<?php
namespace Crux;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Crux\Utils\Path;

abstract class Command extends \Symfony\Component\Console\Command\Command
{
	protected $commandName;
	protected $commandDescription;
	protected $commandHelp;
	
	
	/** @var InputInterface */
	protected $input;
	
	/** @var OutputInterface */
	protected $output;
	
	
	/** @var string */
	protected $cwd;
	
	/** @var string */
	protected $resourceFolder;
	
	
	protected function init()
	{
	}
	
	
	abstract protected function handle();
	
	
	/**
	 *
	 */
	protected function configure()
	{
		if (! $this->getName()) {
			$this->setName($this->commandName);
		}
		
		$this->setDescription($this->commandDescription);
		$this->setHelp($this->commandHelp);
		
		$this->cwd = getcwd();
		$this->resourceFolder = Path::pretty(Path::join(__DIR__, '..', 'resources'));
	}
	
	
	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 *
	 * @return void|null|int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->init();
		
		$this->input = $input;
		$this->output = $output;
		
		$this->handle();
	}
}

<?php
namespace Crux\Vagrant\Command;

use Crux\Command;
use Crux\Utils\File;
use Crux\Utils\Path;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class InitCommand extends Command
{
	protected $commandName = 'init';
	
	
	/** @var QuestionHelper */
	protected $helper;
	
	
	protected function init()
	{
		$this->helper = $this->getHelper('question');
	}
	
	
	protected function handle()
	{
		if ($this->vagrantFileExists()) {
			$this->output->writeln('A Vagrantfile already exists.');
			
			$question = new ConfirmationQuestion('This action will override. Are you sure? (y|n) ', false);
			if (! $this->helper->ask($this->input, $this->output, $question)) {
				$this->output->writeln('No harm has been done!');
				
				return;
			}
		}
		
		$this->vagrantFileCreate();
		
		if (! $this->settingsFileExists()) {
			$question = new ChoiceQuestion('Would you like a JSON or Yaml file? (default: json) ', ['JSON', 'Yaml'], 0);
			
			$type = $this->helper->ask($this->input, $this->output, $question);
			
			$this->settingsFileCreate(strtolower($type));
		}
		
		$this->output->writeln('Crux has been initialized');
	}
	
	
	/**
	 * @return bool
	 */
	protected function vagrantFileExists()
	{
		return Path::exists(Path::join($this->cwd, 'Vagrantfile'));
	}
	
	
	/**
	 * 
	 */
	protected function vagrantFileCreate() {
		$configPath = Path::getRelative($this->cwd, Path::join($this->resourceFolder, 'scripts', 'Crux.rb'));
		
		$vagrantFileData = File::readContent(Path::join($this->resourceFolder, 'templates', 'Vagrantfile'));
		$vagrantFileData = $this->replace('configPath', $configPath, $vagrantFileData);
		
		$vagrantFile = new File(Path::join($this->cwd, 'Vagrantfile'), 'w');
		$vagrantFile->write($vagrantFileData);
		$vagrantFile->close();
	}
	
	
	/**
	 * @return bool
	 */
	protected function settingsFileExists()
	{
		return Path::exists(Path::join($this->cwd, 'Crux.yaml'));
	}
	
	
	/**
	 * @param string $type
	 */
	protected function settingsFileCreate($type)
	{
		copy(Path::join($this->resourceFolder, 'Crux.'.$type), Path::join($this->cwd, 'Crux.'.$type));
	}
	
	
	/**
	 * @param string $variable
	 * @param string $replace
	 * @param string $subject
	 *
	 * @return mixed
	 */
	protected function replace($variable, $replace, $subject)
	{
		return str_replace('{{'.$variable.'}}', $replace, $subject);
	}
}

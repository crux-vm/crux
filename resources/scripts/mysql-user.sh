#!/usr/bin/env bash

mysql --user="root" -e "GRANT ALL PRIVILEGES ON ${1}.* TO '${2}'@'%' IDENTIFIED BY '${3}';"
mysql --user="root" -e "FLUSH PRIVILEGES;"

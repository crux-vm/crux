#!/usr/bin/env bash

block="server {
    listen ${3:-80};
    listen ${4:-443} ssl http2;
    server_name ${1}.dev ${1}.app;
    root \"${2}\";

    index ${5} index.php index.html index.htm;

    charset utf-8;

    access_log off;
    error_log  /var/log/nginx/${1}-error.log error;

    sendfile off;

    client_max_body_size 100m;

    location / {
        try_files \$uri \$uri/ /${5}?\$query_string;
    }

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index ${5};
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;

        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
        fastcgi_connect_timeout 300;
        fastcgi_send_timeout 300;
        fastcgi_read_timeout 300;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }
    location ~ /\.ht { deny all; }

    ssl_certificate     /etc/nginx/ssl/${1}.crt;
    ssl_certificate_key /etc/nginx/ssl/${1}.key;
}
"

echo "$block" > "/etc/nginx/sites-available/${1}"
ln -fs "/etc/nginx/sites-available/${1}" "/etc/nginx/sites-enabled/${1}"

# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'json'
require 'yaml'

class Crux

  NAME = 'crux'
  SCRIPTDIR = File.dirname(__FILE__) # Configure Local Variable To Access Scripts From Remote Location


  def initialize(confDir)
    settingsJson = confDir + "/Crux.json"
    settingsYaml = confDir + "/Crux.yaml"

    if File.exist? settingsJson then
      @settings = JSON.parse(File.read(settingsJson))
    elsif File.exist? settingsYaml then
      @settings = YAML::load(File.read(settingsYaml))
    else
      abort "Crux settings file not found in #{confDir}"
    end
  end


  def configure(config)
    # Setup aliases
    if @settings.include? 'aliases'
      aliasesPath = File.expand_path(@settings['aliases'])

      if File.exist? aliasesPath then
        config.vm.provision "file", name: 'Import aliases', run: 'always', source: aliasesPath, destination: "/home/vagrant/.bash_aliases"
      else
        self.wrongPath(config, 'Import aliases', aliasesPath)
      end
    end

    # Set The VM Provider
    ENV['VAGRANT_DEFAULT_PROVIDER'] = @settings['provider'] ||= 'virtualbox'

    # Prevent TTY Errors
    config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

    # Allow SSH Agent Forward from The Box
    config.ssh.forward_agent = true

    # Box configration
    config.vm.define @settings['name'] ||= Crux::NAME
    config.vm.box = @settings['box'] ||= 'crux-vm/crux'
    
    if @settings.include? 'local'
      config.vm.box_url = @settings['local']
    else
      config.vm.box_version = @settings['version'] ||= '>= 0.1.0'
    end

    # Network configuration
    config.vm.hostname = @settings['hostname'] ||= Crux::NAME
    config.vm.network :private_network, ip: @settings['ip'] ||= '192.168.10.10'

    # config.vm.provision 'shell', name: 'SEL hack', run: 'always', inline: 'setenforce Permissive' # TODO: temporary hack for SEL on nginx
    config.vm.provision 'shell', name: 'Yum update', inline: 'yum update -y > /dev/null 2>&1'

    # Shared folders
    self.folders(config)

    # Sites
    self.sites(config)

    # Databases
    self.databases(config)

    # Provider
    self.virtualbox(config)

    # After script
    if @settings.include? 'after'
      afterScriptPath = File.expand_path(@settings['after'])

      if File.exist? afterScriptPath then
        config.vm.provision 'shell', name: 'After script', path: afterScriptPath, privileged: false
      else
        self.wrongPath(config, 'After script', afterScriptPath)
      end
    end
  end


  protected
  def folders(config)
    config.vm.synced_folder '.', '/vagrant', disabled: true

    if @settings.include? 'folders'
      @settings['folders'].each do |folder|
        if File.exists? File.expand_path(folder['map'])
          config.vm.synced_folder folder['map'], folder['to'], type: folder['type'] ||= nil
        else
          self.wrongPath(config, 'Shared folders', File.expand_path(folder['map']))
        end
      end
    end
  end


  protected
  def sites(config)
    config.vm.provision 'shell', name: 'Clear old Nginx configs', path: Crux::SCRIPTDIR + '/nginx-clear.sh'

    if @settings.include? 'sites'
      @settings['sites'].each do |site|
        # SSL
        config.vm.provision 'shell' do |shell|
          shell.name = 'SSL Certificate for ' + site['name']
          shell.path = Crux::SCRIPTDIR + '/ssl-create.sh'
          shell.args = [site['name']]
        end

        # Nginx
        config.vm.provision 'shell' do |shell|
          shell.name = 'Nginx serving ' + site['name']
          shell.path = Crux::SCRIPTDIR + '/nginx-serve.sh'
          shell.args = [site['name'], site['root'], site['http'] ||= 80, site['https'] ||= 443, site['router'] ||= 'index.php']
        end
      end
    end

    config.vm.provision 'shell', name: 'Restarting Nginx', inline: 'sudo systemctl restart nginx'

    if defined? VagrantPlugins::HostsUpdater
      hosts = @settings['sites'].map { |site| site['name'] + '.app' } + @settings['sites'].map { |site| site['name'] + '.dev' }
      hosts += ['tools.app', 'tools.dev']

      config.hostsupdater.aliases = hosts
    end
  end


  protected
  def databases(config)
    if @settings.include? 'databases'
      @settings['databases'].each do |database|
        # Database
        config.vm.provision 'shell' do |shell|
          shell.name = 'Database: ' + database['name']
          shell.path = Crux::SCRIPTDIR + '/mysql-db.sh'
          shell.args = [database['name']]
        end

        # Users
        if database.include? 'users'
          database['users'].each do |user|
            config.vm.provision 'shell' do |shell|
              shell.name = 'User ' + user[0] + ' for ' + database['name']
              shell.path = Crux::SCRIPTDIR + '/mysql-user.sh'
              shell.args = [database['name'], user[0], user[1]]
            end
          end
        else
          config.vm.provision 'shell' do |shell|
            shell.name = 'User ' + database['name'] + ' for ' + database['name']
            shell.path = Crux::SCRIPTDIR + '/mysql-user.sh'
            shell.args = [database['name'], database['name'], 'secret']
          end
        end
      end
    end
  end


  protected
  def virtualbox(config)
    # Configure A Few VirtualBox Settings
    config.vm.provider "virtualbox" do |vb|
      vb.name = @settings["name"] ||= Crux::NAME
      vb.customize ["modifyvm", :id, "--memory", @settings["memory"] ||= "2048"]
      vb.customize ["modifyvm", :id, "--cpus", @settings["cpus"] ||= "1"]
      vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", @settings["natdnshostresolver"] ||= "on"]
      vb.customize ["modifyvm", :id, "--ostype", "RedHat_64"]

      if @settings.has_key?("gui") && @settings["gui"]
        vb.gui = true
      end
    end
  end


  protected
  def wrongPath(config, name, path)
    config.vm.provision 'shell', name: name, inline: 'echo "File path is incorrect: ${1}"', args: [path]
  end
end

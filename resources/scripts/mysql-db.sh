#!/usr/bin/env bash

mysql --user="root" -e "CREATE DATABASE IF NOT EXISTS ${1} DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci;"
